:: Disable writing the executed commands.
@echo off
:: Try an admin task to find if we have admin privilages.
    IF "%PROCESSOR_ARCHITECTURE%" EQU "amd64" (
>nul 2>&1 "%SYSTEMROOT%\SysWOW64\cacls.exe" "%SYSTEMROOT%\SysWOW64\config\system"
) ELSE (
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
)

:: --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )
:: Elevate the script.
:UACPrompt
:: Create a visual basic script file to elevate this script.
:: Create an object that can elevate a program.
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
:: Rerun the script with admin.
	echo UAC.ShellExecute "cmd.exe", "/c ""%~s0"" %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

:: Execute the created file.
    "%temp%\getadmin.vbs"
:: Clean up of the created elevator
	del "%temp%\getadmin.vbs"
:: Exit this script since another elevated script is running now.
    exit /B

:gotAdmin
:: Change the running directory.
    pushd "%CD%"
    CD /D "%~dp0"
	
::	copys the app's Files
echo Copying Program Files & echo.{
:: Shortcan in the start menu.
xcopy /s /f /q  "%~dp0\Install\Files\eclipse Ide.lnk" "%ALLUSERSPROFILE%\Microsoft\Windows\Start Menu\Programs\"
:: Copy files to programFiles
xcopy /s /e /q /-y "%~dp0\Install\Files\eclipse" "%ProgramFiles%\eclipse\"
:: copy uninstall to the install directory.
xcopy /s /f /q "%~dp0\Install\Files\uninstall.bat" "%ProgramFiles%\eclipse\"
echo }& echo.Done! & echo.____________________________________________

::	adds Path to app, register the app as a recognizable program.
echo Adding Program to Path & echo.{
:: Add a key for the program
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\App Paths\eclipse.exe"
:: Add a the installation location to path.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\App Paths\eclipse.exe" /v "Path" /t REG_SZ /d "C:\Program Files\eclipse"
:: Add executable path to the information at the registery.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\App Paths\eclipse.exe" /v "" /t REG_SZ /d "C:\Program Files\eclipse\eclipse.exe"
echo }& echo.Done! & echo.____________________________________________

::	adds Path to installation/uninstallations
echo Adding Windows App Info to the Program & echo.{
:: Add an unstall key for the program.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse"
:: Add a display icon to the program details.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "DisplayIcon" /t REG_SZ /d "C:\Program Files\eclipse\eclipse.exe"
:: Add a display name to the program.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "DisplayName" /t REG_SZ /d "Eclipse Ide"
:: Add a display version to the program.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "DisplayVersion" /t REG_SZ /d "4.5.2"

:: Check how big the package weights for estimated size.
setlocal enableextensions disabledelayedexpansion

    set "target=%~1"
	
    if not defined target set "target=%cd%"

    set "size=0"
    for /f "tokens=3,5" %%a in ('
        dir /a /s /w /-c "%target%"
        ^| findstr /b /l /c:"  "
    ') do if "%%b"=="" set "size=%%a"
	set /a "size=size/1024"
:: Add the esimated size to the program details.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "EstimatedSize" /t REG_DWORD /d %size%
:: Add a website to the program details.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "HelpLink" /t REG_SZ /d "eclipse.org"
:: Add an install location to the program's details.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "InstallLocation" /t REG_SZ /d "%ProgramFiles%\eclipse\%"
:: Mark the software as unable to modify
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "NoModify" /t REG_DWORD /d 1
:: Mark the software as unable to repair.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "NoRepair" /t REG_DWORD /d 1
:: Add the app publisher to the program details.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "Publisher" /t REG_SZ /d "Eclipse Foundation"
:: Ignore this registery key.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "sEstimatedSize2" /t REG_DWORD /d 0
:: Add an uninstall command to the program details.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "UninstallString" /t REG_SZ /d "cmd /k \"%ProgramFiles%\eclipse\uninstall.bat\""
:: Add the major version number to the program details.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "VersionMajor" /t REG_DWORD /d 4
:: Add the minor version number to the program details.
REG ADD "HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\eclipse" /v "VersionMinor" /t REG_DWORD /d 52
:: Add an execute command key to the registery.
REG ADD "HKCR\Applicaions\eclipse.exe\shell\open\command"
:: Add an execute command to the key added previously.
REG ADD "HKCR\Applicaions\eclipse.exe\shell\open\command" /v "" /t REG_SZ /d "C:\Program Files\eclipse\eclipse.exe \"%1\""
echo } & echo.Done! & echo.____________________________________________
PAUSE